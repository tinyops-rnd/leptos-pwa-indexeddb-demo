# Leptos PWA IndexedDB Demo

## How to run

```shell
rustup target add wasm32-unknown-unknown
cargo install trunk
trunk serve
```

## Build PWA

```shell
trunk build --release
```

Copy `service_worker.js`, `manifest.json`, `icon-256.png` and `manifest.json` to `dist`. 

Edit `service_worker.js`, fix filenames.