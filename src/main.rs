use std::fmt::{Display, Formatter};
use leptos::*;

use serde::{Serialize, Deserialize};

use deli::{Database, Transaction, Model, Error};
use log::info;

#[derive(Serialize, Deserialize, Clone, Model, Debug)]
pub struct User {
    #[deli(auto_increment)]
    pub id: u32,
    pub name: String,
    pub email: String,
    pub tags: Vec<String>
}

impl Display for User {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "user name '{}', email '{}'", self.name, self.email)
    }
}

#[component]
fn App(cx: Scope) -> impl IntoView {
    let once = create_resource(cx, || (), |_| async move {
        info!("creating db..");
        let db = create_database().await.unwrap();
        info!("db created");

        let wt = create_write_transaction(&db).await.unwrap();
        let user_id = add_employee(&wt).await.unwrap();

        let rt = create_read_transaction(&db).await.expect("couldn't create rtx");
        info!("employee has been added, id {user_id}");

        let user = get_employee(&rt, user_id).await.unwrap();

        info!("user has been loaded by id {user_id}");

        user
    }
    );

    view! { cx,
        <div>
            <h2>"LEPTOS PWA"</h2>

            <div>
                {move || match once.read(cx) {
                    None => view! { cx, <p>"Loading..."</p> }.into_view(cx),
                    Some(data) => {
                        match data {
                            Some(user) => view! { cx,
                                <div>
                                    <div>{user.to_string()}</div>
                                    <div>
                                        {user.tags.into_iter()
                                            .map(|tag| view! { cx, <li>{tag}</li>})
                                            .collect::<Vec<_>>()}</div>
                                    </div>
                            }.into_view(cx),
                            None => view! { cx, <div>"empty"</div> }.into_view(cx)
                        }
                    }
                }}
            </div>
        </div>
    }
}

async fn create_database() -> Result<Database, Error> {
    let database = Database::builder("test_db".to_string(), 1)
                            .register_model::<User>().build().await.unwrap();

    Ok(database)
}

async fn create_read_transaction(database: &Database) -> Result<Transaction, Error> {
    Ok(database.transaction().with_model::<User>().build().unwrap())
}

async fn create_write_transaction(database: &Database) -> Result<Transaction, Error> {
    Ok(database.transaction().writable().with_model::<User>().build().unwrap())
}

async fn add_employee(transaction: &Transaction) -> Result<u32, Error> {
    let tags: Vec<String> = vec!["a".to_string(), "b".to_string(), "c".to_string()];
    User::with_transaction(transaction)?.add("Alice", "alice@example.com", &tags).await
}

async fn get_employee(transaction: &Transaction, id: u32) -> Result<Option<User>, Error> {
    Ok(User::with_transaction(transaction)?.get(&id).await.unwrap())
}

fn main() {
    _ = console_log::init_with_level(log::Level::Debug);
    console_error_panic_hook::set_once();

    info!("LEPTOS PWA AND INDEXEDDB DEMO");

    mount_to_body(|cx| view! { cx,  <App/> })
}
