var cacheName = 'yew-pwa';

var filesToCache = [
    './',
    './index.html',
    './icon-256.png',
    './leptos-pwa-d1da4030a3350f4.js',
    './leptos-pwa-d1da4030a3350f4_bg.wasm',
    './manifest.json',
];


/* Start the service worker and cache all of the app's content */
self.addEventListener('install', function(e) {
    e.waitUntil(
        caches.open(cacheName).then(function(cache) {
            return cache.addAll(filesToCache);
        })
    );
});

/* Serve cached content when offline */
self.addEventListener('fetch', function(e) {
    e.respondWith(
        caches.match(e.request).then(function(response) {
            return response || fetch(e.request);
        })
    );
});